﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lab6_PageFactory
{
    [TestFixture]
    class Test
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

        }

        [TearDown]
        public void cleanup()
        {
            driver.Quit();
        }

        [Test]
        public void CheckLogin()
        {
            try
            {
                InitialPage page = new InitialPage(driver);
                page.goToPage();
                page.button[1].SendKeys(Keys.Enter);
                Thread.Sleep(5000);
                //page.CallModalWindow();
                ModalWindow window = new ModalWindow(page.form);
                window.CheckForm();
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                driver.Close();
                Console.WriteLine(e.Message);
            }
        }
    }
}
