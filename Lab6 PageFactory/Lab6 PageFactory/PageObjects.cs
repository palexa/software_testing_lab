﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab6_PageFactory
{
    class InitialPage
    {
        private IWebDriver driver;

        public InitialPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "account-button")]
        public List <IWebElement> button;

        [FindsBy(How = How.ClassName, Using = "login")]
        public IWebElement form;

        public void CallModalWindow()
        {
            button[1].SendKeys(Keys.Enter);
            Thread.Sleep(5000);

        }

        public void goToPage()
        {
            driver.Navigate().GoToUrl("https://www.momondo.by/");
        }
    }

    class ModalWindow
    {
        private IWebDriver SubmitButton;
        public ModalWindow(IWebElement Page)
        {
            PageFactory.InitElements(Page, this);
        }

        [FindsBy(How = How.ClassName, Using = "Common-Authentication-LoginForm")]
        public IWebElement Modal;

        public void CheckForm()
        {
            IWebElement SubmitButton = this.Modal.FindElement(By.ClassName("Common-Widgets-Button-ButtonDeprecated"));
            SubmitButton.SendKeys(Keys.Enter);
        }
    }
    
}
