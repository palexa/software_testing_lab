﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_lab_one
{
    class Student
    {
        private List<int> marks = new List<int>();
        private string name;

        public string GetName()
        {
            return name;
        }

        public Student(string name,List<int> marks)
        {
            this.name = name;
            this.marks = marks;
        }

        public float AverageMark() => (float)marks.Average();
    }
}
