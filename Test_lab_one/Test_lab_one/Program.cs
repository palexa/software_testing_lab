﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_lab_one
{
    class Program
    {
        static void Main(string[] args)
        {
            //commit one
            //commit two
            //commit three
            Student one = new Student("Leha",new List<int>() { 1, 3, 5, 5 });
            Student two = new Student("Leha2", new List<int>() { 1, 2, 3, 5 });
            Student three = new Student("Leha3", new List<int>() { 9, 2, 3, 5 });
            Student four = new Student("Leha4", new List<int>() { 8, 10, 9, 5 });
            List<Student> list = new List<Student>() { one, two, three, four };
            Group second = new Group("Second",list);
            foreach(var x in list)
            {
                Console.WriteLine($"Average mark of {x.GetName()}: " + x.AverageMark());
            }
            Console.WriteLine($"Average mark of {second.GetName()} group: {second.AverageMarkOfStudents()}");
            Console.ReadKey();
            //commit four
            //commit five
            //commit six
            //commit seven
            //commit eight
        }
    }
}
