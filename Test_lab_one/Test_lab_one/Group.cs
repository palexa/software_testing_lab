﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_lab_one
{
    class Group
    {
        private List<Student> students = new List<Student>();
        private string name;

        public Group(string name, List<Student> students)
        {
            this.name = name;
            this.students = students;
        }

        public string GetName()
        {
            return name;
        }

        public float AverageMarkOfStudents()
        {
            return students.Select(x => x.AverageMark()).Average(); 
        }
    }
}
