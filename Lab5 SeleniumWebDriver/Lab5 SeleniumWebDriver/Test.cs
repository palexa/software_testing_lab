﻿using OpenQA.Selenium;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Lab5_SeleniumWebDriver
{
    [TestFixture]
    class Test
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

        }

        [TearDown]
        public void cleanup()
        {
            driver.Quit();
        }

        [Test]
        public void SearchWithValidData()
        {
            try
            {
                driver.Navigate().GoToUrl("https://www.momondo.by/");
                IWebElement dest = driver.FindElement(By.ClassName("destinationBlock"));
                IWebElement destinationInput = dest.FindElement(By.ClassName("Common-Widgets-Text-TextInput"));
                destinationInput.SendKeys("TestStringTestStringTestStringTestStringTestString");
                //destinationInput.SendKeys(Keys.Enter);
                Thread.Sleep(10000);
                IWebElement searchForm = driver.FindElement(By.ClassName("searchform "));
                searchForm.Submit();
                Thread.Sleep(5000);
              
            }
            catch (Exception e)
            {
                driver.Close();
                Console.WriteLine(e.Message);
            }
        }
    }
}
